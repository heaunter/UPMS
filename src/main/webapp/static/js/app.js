$(function() {

	// 分页
	var $pageNum = $("#pageNum");
	$pageNum.val('${ page.pageNumber!}');
	$('.page').click(function() {
		var ac = $(this).attr('tag');
		$pageNum.val(ac);
		$("#queryForm").submit();
	});

});

Utils = {
		basePath:(function(){
			var curWwwPath = window.document.location.href;
			//获取主机地址之后的目录，如： GameFngine/meun.jsp
			var pathName = window.document.location.pathname;
			var pos = curWwwPath.indexOf(pathName);
			//获取主机地址，如： http://localhost:8080
			var localhostPaht = curWwwPath.substring(0, pos);
			//获取带"/"的项目名，如：/GameFngine
			var projectName = pathName.substring(0,
				pathName.substr(1).indexOf('/') + 1);
			return (localhostPaht + projectName + "/");
		})(),
		setForm: function($form, data) {
			$form.find('[name]').each(function(i, obj) {
				var val = this.value;
				if ('text' == obj.type || 'SELECT' == obj.tagName || 'hidden' == obj.type) {
					this.value = data[obj.name]?data[obj.name]:'';
				} else if ('radio' == obj.type) {
					if (val == data[obj.name]) {
						this.checked = true;
					}
				} else if ('TEXTAREA'  == obj.tagName) {
					this.value = data[obj.name]?data[obj.name]:'';
				}
			});
		},
		/**
		 * url 获取列表数据的请求地址
		 * params 请求参数
		 * id  传递value值
		 * name 显示值
		 * targetId 选中结果放置Box
		 * checkedVal 默认选中值列表
		 */
		setOptions : function(url, params,id,name, targetId, checkedVal){
			$.post(url, params,function(rs) {
				var $targetId = $(targetId);
				$targetId.empty();
				var $option = $('<option value="">--请选择--</option>');
				$targetId.append($option);
				if(rs){
					for (var j = 0; j < rs.length; j++) {
						$option = $('<option value="'+rs[j][id]+'">'+rs[j][name]+'</option>');
						if(checkedVal && checkedVal == rs[j][id]){
							$option.attr('selected','selected');
						}
						$targetId.append($option);
					}
				}
			});
		},
		// 邮编
		postCode:/^[1-9][0-9]{5}$/,
		// 正整数
		positiveInteger:/^[0-9]*$/,
		//正负数
		positiveNegativeNumber:/^[+-]?\d*\.?\d{1,2}$/,
		// 邮箱
		emailReg: /^([a-zA-Z0-9]+[_|\_|\.-]?)*[a-zA-Z0-9]+@([a-zA-Z0-9]+[_|\_|\.-]?)*[a-zA-Z0-9]+\.[a-zA-Z]{2,3}$/,
		// 中文
		chineseReg: /[^\x00-\xff]/g,
		//JavaScript
		scriptReg:/<script.*?>.*?<\/script>/ig,
		// 用户名
		userNameReg: /^[a-z0-9]+\_{0,1}[a-z0-9]+$/,
		// 价格
		priceReg: /^[0-9]+\.{0,1}[0-9]{0,2}$/,
		validation: function ($form) {
			var array = $form.serializeArray();
			if (array && 0 < array.length) {
				for (i = 0; i < array.length; i++) {
					var val = array[i].value;
					var name = array[i].name.toLowerCase();
					if ('category'== name && '--请选择--' == val) {
						Utils.alert('类型不能为空！');
						return false;
					} else if ('code' == name && (!val || /\W+/.test(val))) {
						Utils.alert('代码不能为空！格式：(数字和字母(大写))');
						return false;
					} else if (-1 != name.indexOf('name') && !val) {
						Utils.alert('名称不能为空！');
						return false;
					} else if (-1 != name.indexOf('email') && !this.emailReg.test(val)) {
						Utils.alert('邮箱格式错误！');
						return false;
					} else if ((-1 != name.indexOf('price') || -1 != name.indexOf('availrate')) && !this.priceReg.test(val)) {
						if ('salesprice' != name) {
							Utils.alert('请输入正确的价格！');
							return false;
						} else if ('salesprice' == name && val) {
							Utils.alert('请输入正确的价格！');
							return false;
						}
					}
				}
			}
			return true;
		}
	};


Date.prototype.Format = function (fmt) { //author: meizz 
    var o = {
        "M+": this.getMonth() + 1, //月份 
        "d+": this.getDate(), //日 
        "h+": this.getHours(), //小时 
        "m+": this.getMinutes(), //分 
        "s+": this.getSeconds(), //秒 
        "q+": Math.floor((this.getMonth() + 3) / 3), //季度 
        "S": this.getMilliseconds() //毫秒 
    };
    if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    for (var k in o)
    if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
    return fmt;
}
