package com.sutaitech.service.system.impl;

import java.util.List;

import org.beetl.sql.core.SQLManager;
import org.beetl.sql.ext.spring.SpringBeetlSql;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sutaitech.entity.SysUser;
import com.sutaitech.service.system.SysUserService;
import com.sutaitech.utils.PageInfo;
import com.sutaitech.utils.ResultVo;

@Service
public class SysUserServiceImpl implements SysUserService{
	
	@Autowired
	SpringBeetlSql beetlsql;

	@Override
	public List<SysUser> findAll() {
		SQLManager dao = beetlsql.getSQLMananger();
		List<SysUser> list =dao.all(SysUser.class);
		return list;
	}

	public PageInfo<SysUser> findAllByPage(PageInfo<SysUser> pageInfo) {        
        SQLManager dao = beetlsql.getSQLMananger();
		List<SysUser> list =dao.all(SysUser.class,pageInfo.getLimitStart(),pageInfo.getLimit());
		
		PageInfo<SysUser> page = new PageInfo<SysUser>();    
	    page.setTotal((int)dao.allCount(SysUser.class));
		page.setList(list);
        return page;
    }

    public ResultVo addSysUser(SysUser entity) {
    	ResultVo vo = new ResultVo();
        try {
        	 SQLManager dao = beetlsql.getSQLMananger();
        	 dao.insert(SysUser.class,entity);
		} catch (Exception e) {
			vo.setStatus(500);
			vo.setMessage("保存失败");
		}
        return vo;
    }

    public SysUser findById(int id) {
    	SysUser vo = null;
    	try {
    		SQLManager dao = beetlsql.getSQLMananger();
    		vo=dao.unique(SysUser.class, id);
		} catch (Exception e) {
			e.printStackTrace();
		}
        return vo;
    }

    public ResultVo updateSysUser(SysUser entity) {
    	ResultVo vo = new ResultVo();
        try {
        	SQLManager dao = beetlsql.getSQLMananger();
        	dao.updateById(entity);
		} catch (Exception e) {
			vo.setStatus(500);
			vo.setMessage("保存失败");
		}
        return vo;
    }

    public ResultVo deleteSysUser(int id) {
    	ResultVo vo = new ResultVo();
        try {
        	SQLManager dao = beetlsql.getSQLMananger();
        	dao.deleteById(SysUser.class, id);
        } catch (Exception e) {
			vo.setStatus(500);
			vo.setMessage("删除失败");
		}
        return vo;
    }

}
