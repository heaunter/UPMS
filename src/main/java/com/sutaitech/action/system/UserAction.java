package com.sutaitech.action.system;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sutaitech.action.BaseAction;
import com.sutaitech.service.system.SysUserService;
import com.sutaitech.entity.SysUser;
import com.sutaitech.utils.PageInfo;
import com.sutaitech.utils.ResultVo;

@Controller
@RequestMapping("/system/user")
public class UserAction extends BaseAction {
    
    @Autowired
    private SysUserService sysUserServiceImpl;

    @RequestMapping(value = {"", "/"})
    public String page(ModelMap map,@ModelAttribute PageInfo<SysUser> pageInfo) {
        PageInfo<SysUser> res = sysUserServiceImpl.findAllByPage(pageInfo);
        map.put("page", res);
   		return "/system/user/index";
   	}
    
    /**
	 * 跳转新增页面
	 */
	@RequestMapping(value = "/addJump")
	public String addJump(ModelMap map){
		return "/system/user/add";
	}
	
	/**
	 * 跳转编辑页面
	 */
	@RequestMapping(value = "/editJump/{id}")
	public String editJump(ModelMap map, @PathVariable String id) {
		SysUser user = sysUserServiceImpl.findById(Integer.parseInt(id));
		map.put("user", user);
		return "/system/user/edit";
	}
	
	@RequestMapping(value = "/add")
	@ResponseBody
    public ResultVo add(ModelMap map,SysUser user) {
		ResultVo vo =sysUserServiceImpl.addSysUser(user);
   		return vo;
   	}
	 
	@RequestMapping(value = "/edit")
	@ResponseBody
    public ResultVo edit(ModelMap map,SysUser user) {
		ResultVo vo =sysUserServiceImpl.updateSysUser(user);
   		return vo;
   	}
	
	@RequestMapping(value = "/delete")
	@ResponseBody
    public ResultVo delete(ModelMap map,int id) {
		ResultVo vo =sysUserServiceImpl.deleteSysUser(id);
   		return vo;
   	}
}
